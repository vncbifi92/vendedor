class Produtos {
  Data _data;

  Produtos({Data data}) {
    this._data = data;
  }

  Data get data => _data;
  set data(Data data) => _data = data;

  Produtos.fromJson(Map<String, dynamic> json) {
    _data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._data != null) {
      data['data'] = this._data.toJson();
    }
    return data;
  }
}

class Data {
  List<Produto> _produtos;

  Data({List<Produto> produtos}) {
    this._produtos = produtos;
  }

  List<Produto> get produtos => _produtos;
  set produtos(List<Produto> produtos) => _produtos = produtos;

  Data.fromJson(Map<String, dynamic> json) {
    if (json['produtos'] != null) {
      _produtos = new List<Produto>();
      json['produtos'].forEach((v) {
        _produtos.add(new Produto.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._produtos != null) {
      data['produtos'] = this._produtos.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Produto {
  String _foto;
  String _titulo;
  String _nota;
  String _data;

  Produto({String foto, String titulo, String nota, String data}) {
    this._foto = foto;
    this._titulo = titulo;
    this._nota = nota;
    this._data = data;
  }

  String get foto => _foto;
  set foto(String foto) => _foto = foto;
  String get titulo => _titulo;
  set titulo(String titulo) => _titulo = titulo;
  String get nota => _nota;
  set nota(String nota) => _nota = nota;
  String get data => _data;
  set data(String data) => _data = data;

  Produto.fromJson(Map<String, dynamic> json) {
    _foto = json['foto'];
    _titulo = json['titulo'];
    _nota = json['nota'];
    _data = json['data'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['foto'] = this._foto;
    data['titulo'] = this._titulo;
    data['nota'] = this._nota;
    data['data'] = this._data;
    return data;
  }
}