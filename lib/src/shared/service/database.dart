import 'package:moor_flutter/moor_flutter.dart';
part 'database.g.dart';//flutter packages pub run build_runner build

class Usuario extends Table{
  IntColumn get id => integer().autoIncrement()();
  TextColumn get nome => text().withLength(min: 3, max: 100)();
  TextColumn get senha => text().withLength(min: 3, max: 100)();
  TextColumn get email => text().withLength(min: 3, max: 100)();
}

class Produtos extends Table{
  IntColumn get id => integer().autoIncrement()();
  IntColumn get usuarioId => integer()();
  DateTimeColumn get data => dateTime()();
  TextColumn get titulo => text().withLength(max: 200)();
  TextColumn get nota => text().withLength(max: 4000)();
  TextColumn get foto => text().withLength(max: 4000)();
}

@UseMoor(tables: [Usuario, Produtos])
class MyDatabase extends _$MyDatabase{

  static final MyDatabase instance = MyDatabase._internal();

  MyDatabase._internal() : super(FlutterQueryExecutor.inDatabaseFolder(path: 'db.sqlite'));


  Future<List<UsuarioData>> getUsuario(String nome, String senha){
    print("getUsuario:$nome, $senha");
    return (select(usuario)..where((Usuario u) => u.nome.equals(nome))..where((Usuario u) => u.senha.equals(senha))).get();
  }

  Future<int> addUsuario(UsuarioData u){
    print("addUsuario:${u.toString()}");
    return into(usuario).insert(u);
  }

  Stream<List<Produto>> getProdutos(int usuario){
    print("getProduto:$usuario");
    Stream<List<Produto>> streamProdutos = (select(produtos)..where((Produtos p) => p.usuarioId.equals(usuario))).watch();
    return streamProdutos;
  }

  Future<List<Produto>> getProdutosPorNome(String nome)async{
    print("getProduto:$nome");
    List<Produto> streamProdutos = await (select(produtos)..where((Produtos p) => p.titulo.like("%"+nome+"%"))).get();
    return streamProdutos;
  }

  Future<int> addProduto(Produto p){
    print("addProduto:${p.toString()}");
    return into(produtos).insert(p);
  }

  Future<int> removeProduto(Produto p){
    print("removeProduto:${p.toString()}");
    return delete(produtos).delete(p);
  }


  @override
  int get schemaVersion => 1;
}
