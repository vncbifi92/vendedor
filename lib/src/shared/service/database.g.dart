// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps
class UsuarioData {
  final int id;
  final String nome;
  final String senha;
  final String email;
  UsuarioData({this.id, this.nome, this.senha, this.email});
  factory UsuarioData.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return UsuarioData(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      nome: stringType.mapFromDatabaseResponse(data['${effectivePrefix}nome']),
      senha:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}senha']),
      email:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}email']),
    );
  }
  factory UsuarioData.fromJson(Map<String, dynamic> json) {
    return UsuarioData(
      id: json['id'] as int,
      nome: json['nome'] as String,
      senha: json['senha'] as String,
      email: json['email'] as String,
    );
  }
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'nome': nome,
      'senha': senha,
      'email': email,
    };
  }

  UsuarioData copyWith({int id, String nome, String senha, String email}) =>
      UsuarioData(
        id: id ?? this.id,
        nome: nome ?? this.nome,
        senha: senha ?? this.senha,
        email: email ?? this.email,
      );
  @override
  String toString() {
    return (StringBuffer('UsuarioData(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('senha: $senha, ')
          ..write('email: $email')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      $mrjc($mrjc($mrjc(0, id.hashCode), nome.hashCode), senha.hashCode),
      email.hashCode));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is UsuarioData &&
          other.id == id &&
          other.nome == nome &&
          other.senha == senha &&
          other.email == email);
}

class $UsuarioTable extends Usuario with TableInfo<$UsuarioTable, UsuarioData> {
  final GeneratedDatabase _db;
  final String _alias;
  $UsuarioTable(this._db, [this._alias]);
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    var cName = 'id';
    if (_alias != null) cName = '$_alias.$cName';
    return GeneratedIntColumn('id', $tableName, false, hasAutoIncrement: true);
  }

  GeneratedTextColumn _nome;
  @override
  GeneratedTextColumn get nome => _nome ??= _constructNome();
  GeneratedTextColumn _constructNome() {
    var cName = 'nome';
    if (_alias != null) cName = '$_alias.$cName';
    return GeneratedTextColumn('nome', $tableName, false,
        minTextLength: 3, maxTextLength: 100);
  }

  GeneratedTextColumn _senha;
  @override
  GeneratedTextColumn get senha => _senha ??= _constructSenha();
  GeneratedTextColumn _constructSenha() {
    var cName = 'senha';
    if (_alias != null) cName = '$_alias.$cName';
    return GeneratedTextColumn('senha', $tableName, false,
        minTextLength: 3, maxTextLength: 100);
  }

  GeneratedTextColumn _email;
  @override
  GeneratedTextColumn get email => _email ??= _constructEmail();
  GeneratedTextColumn _constructEmail() {
    var cName = 'email';
    if (_alias != null) cName = '$_alias.$cName';
    return GeneratedTextColumn('email', $tableName, false,
        minTextLength: 3, maxTextLength: 100);
  }

  @override
  List<GeneratedColumn> get $columns => [id, nome, senha, email];
  @override
  $UsuarioTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'usuario';
  @override
  final String actualTableName = 'usuario';
  @override
  bool validateIntegrity(UsuarioData instance, bool isInserting) =>
      id.isAcceptableValue(instance.id, isInserting) &&
      nome.isAcceptableValue(instance.nome, isInserting) &&
      senha.isAcceptableValue(instance.senha, isInserting) &&
      email.isAcceptableValue(instance.email, isInserting);
  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  UsuarioData map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return UsuarioData.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(UsuarioData d,
      {bool includeNulls = false}) {
    final map = <String, Variable>{};
    if (d.id != null || includeNulls) {
      map['id'] = Variable<int, IntType>(d.id);
    }
    if (d.nome != null || includeNulls) {
      map['nome'] = Variable<String, StringType>(d.nome);
    }
    if (d.senha != null || includeNulls) {
      map['senha'] = Variable<String, StringType>(d.senha);
    }
    if (d.email != null || includeNulls) {
      map['email'] = Variable<String, StringType>(d.email);
    }
    return map;
  }

  @override
  $UsuarioTable createAlias(String alias) {
    return $UsuarioTable(_db, alias);
  }
}

class Produto {
  final int id;
  final int usuarioId;
  final DateTime data;
  final String titulo;
  final String nota;
  final String foto;
  Produto(
      {this.id, this.usuarioId, this.data, this.titulo, this.nota, this.foto});
  factory Produto.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    final stringType = db.typeSystem.forDartType<String>();
    return Produto(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      usuarioId:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}usuario_id']),
      data:
          dateTimeType.mapFromDatabaseResponse(data['${effectivePrefix}data']),
      titulo:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}titulo']),
      nota: stringType.mapFromDatabaseResponse(data['${effectivePrefix}nota']),
      foto: stringType.mapFromDatabaseResponse(data['${effectivePrefix}foto']),
    );
  }
  factory Produto.fromJson(Map<String, dynamic> json) {
    return Produto(
      id: json['id'] as int,
      usuarioId: json['usuarioId'] as int,
      data: json['data'] as DateTime,
      titulo: json['titulo'] as String,
      nota: json['nota'] as String,
      foto: json['foto'] as String,
    );
  }
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'usuarioId': usuarioId,
      'data': data,
      'titulo': titulo,
      'nota': nota,
      'foto': foto,
    };
  }

  Produto copyWith(
          {int id,
          int usuarioId,
          DateTime data,
          String titulo,
          String nota,
          String foto}) =>
      Produto(
        id: id ?? this.id,
        usuarioId: usuarioId ?? this.usuarioId,
        data: data ?? this.data,
        titulo: titulo ?? this.titulo,
        nota: nota ?? this.nota,
        foto: foto ?? this.foto,
      );
  @override
  String toString() {
    return (StringBuffer('Produto(')
          ..write('id: $id, ')
          ..write('usuarioId: $usuarioId, ')
          ..write('data: $data, ')
          ..write('titulo: $titulo, ')
          ..write('nota: $nota, ')
          ..write('foto: $foto')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      $mrjc(
          $mrjc(
              $mrjc($mrjc($mrjc(0, id.hashCode), usuarioId.hashCode),
                  data.hashCode),
              titulo.hashCode),
          nota.hashCode),
      foto.hashCode));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is Produto &&
          other.id == id &&
          other.usuarioId == usuarioId &&
          other.data == data &&
          other.titulo == titulo &&
          other.nota == nota &&
          other.foto == foto);
}

class $ProdutosTable extends Produtos with TableInfo<$ProdutosTable, Produto> {
  final GeneratedDatabase _db;
  final String _alias;
  $ProdutosTable(this._db, [this._alias]);
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    var cName = 'id';
    if (_alias != null) cName = '$_alias.$cName';
    return GeneratedIntColumn('id', $tableName, false, hasAutoIncrement: true);
  }

  GeneratedIntColumn _usuarioId;
  @override
  GeneratedIntColumn get usuarioId => _usuarioId ??= _constructUsuarioId();
  GeneratedIntColumn _constructUsuarioId() {
    var cName = 'usuario_id';
    if (_alias != null) cName = '$_alias.$cName';
    return GeneratedIntColumn(
      'usuario_id',
      $tableName,
      false,
    );
  }

  GeneratedDateTimeColumn _data;
  @override
  GeneratedDateTimeColumn get data => _data ??= _constructData();
  GeneratedDateTimeColumn _constructData() {
    var cName = 'data';
    if (_alias != null) cName = '$_alias.$cName';
    return GeneratedDateTimeColumn(
      'data',
      $tableName,
      false,
    );
  }

  GeneratedTextColumn _titulo;
  @override
  GeneratedTextColumn get titulo => _titulo ??= _constructTitulo();
  GeneratedTextColumn _constructTitulo() {
    var cName = 'titulo';
    if (_alias != null) cName = '$_alias.$cName';
    return GeneratedTextColumn('titulo', $tableName, false, maxTextLength: 200);
  }

  GeneratedTextColumn _nota;
  @override
  GeneratedTextColumn get nota => _nota ??= _constructNota();
  GeneratedTextColumn _constructNota() {
    var cName = 'nota';
    if (_alias != null) cName = '$_alias.$cName';
    return GeneratedTextColumn('nota', $tableName, false, maxTextLength: 4000);
  }

  GeneratedTextColumn _foto;
  @override
  GeneratedTextColumn get foto => _foto ??= _constructFoto();
  GeneratedTextColumn _constructFoto() {
    var cName = 'foto';
    if (_alias != null) cName = '$_alias.$cName';
    return GeneratedTextColumn('foto', $tableName, false, maxTextLength: 4000);
  }

  @override
  List<GeneratedColumn> get $columns =>
      [id, usuarioId, data, titulo, nota, foto];
  @override
  $ProdutosTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'produtos';
  @override
  final String actualTableName = 'produtos';
  @override
  bool validateIntegrity(Produto instance, bool isInserting) =>
      id.isAcceptableValue(instance.id, isInserting) &&
      usuarioId.isAcceptableValue(instance.usuarioId, isInserting) &&
      data.isAcceptableValue(instance.data, isInserting) &&
      titulo.isAcceptableValue(instance.titulo, isInserting) &&
      nota.isAcceptableValue(instance.nota, isInserting) &&
      foto.isAcceptableValue(instance.foto, isInserting);
  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Produto map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Produto.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(Produto d, {bool includeNulls = false}) {
    final map = <String, Variable>{};
    if (d.id != null || includeNulls) {
      map['id'] = Variable<int, IntType>(d.id);
    }
    if (d.usuarioId != null || includeNulls) {
      map['usuario_id'] = Variable<int, IntType>(d.usuarioId);
    }
    if (d.data != null || includeNulls) {
      map['data'] = Variable<DateTime, DateTimeType>(d.data);
    }
    if (d.titulo != null || includeNulls) {
      map['titulo'] = Variable<String, StringType>(d.titulo);
    }
    if (d.nota != null || includeNulls) {
      map['nota'] = Variable<String, StringType>(d.nota);
    }
    if (d.foto != null || includeNulls) {
      map['foto'] = Variable<String, StringType>(d.foto);
    }
    return map;
  }

  @override
  $ProdutosTable createAlias(String alias) {
    return $ProdutosTable(_db, alias);
  }
}

abstract class _$MyDatabase extends GeneratedDatabase {
  _$MyDatabase(QueryExecutor e) : super(const SqlTypeSystem.withDefaults(), e);
  $UsuarioTable _usuario;
  $UsuarioTable get usuario => _usuario ??= $UsuarioTable(this);
  $ProdutosTable _produtos;
  $ProdutosTable get produtos => _produtos ??= $ProdutosTable(this);
  @override
  List<TableInfo> get allTables => [usuario, produtos];
}
