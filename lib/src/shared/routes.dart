import 'package:flutter/material.dart';
import 'package:vendedor/src/index/index-widget.dart';
import 'package:vendedor/src/produtos/busca/busca-widget.dart';
import 'package:vendedor/src/produtos/index-widget.dart';
import 'package:vendedor/src/produtos/novos/novos-widget.dart';

final routes = {
  '/': (BuildContext context) => IndexPage(),
  '/produtosIndex': (BuildContext context) => ProdsPage(),
  '/novoProduto': (BuildContext context) => NovoProdPage(),
  '/produtoBusca': (BuildContext context) => BuscaProdsPage(),
};