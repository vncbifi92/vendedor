import 'package:flutter/material.dart';
import 'package:vendedor/src/index/login/login-bloc.dart' as loginBloc;
import 'package:vendedor/src/index/registro/registro-bloc.dart' as registroBloc;
import 'package:vendedor/src/index/index-bloc.dart' as indexBloc;
import 'package:vendedor/src/index/login/login-widget.dart';
import 'package:vendedor/src/index/registro/registro-widget.dart';

class IndexPage extends StatefulWidget {
  IndexPage({Key key}) : super(key: key);

  @override
  _IndexPageState createState() => new _IndexPageState();
}

class _IndexPageState extends State<IndexPage> with TickerProviderStateMixin{

  Widget _logo(){

    return StreamBuilder<Object>(
      stream: indexBloc.indexBloc.outAnimar,
      builder: (context, snapshot) {
        if(snapshot.hasData) {

          return AnimatedSize(
            curve: Curves.linear,
              duration: Duration(milliseconds: 500),
              vsync: this,
              child:
              Image(
                image: snapshot.data ? AssetImage("assets/images/03.png") : AssetImage("assets/images/02.png"),
                height: snapshot.data ? 71 : 214,
                width: snapshot.data ? 117 : 352,
              )
          );

        }else{
          return Image(
            image: AssetImage("assets/images/03.png"),
            height: 214,
            width: 352,
          );
        }
      }
    );
  }

  Widget _index(){
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          StreamBuilder(
            stream: registroBloc.registroBloc.outAnimar,
            builder: (context, snapshot) {
              if(snapshot.hasData) {
                return snapshot.data ? Center() : widgetLogin.animaLogin();
              }else{
                return Container();
              }
            }
          ),
          StreamBuilder(
            stream: loginBloc.loginBloc.outAnimar,
            builder: (context, snapshot) {
              if(snapshot.hasData) {
                return snapshot.data ? Center() : widgetRegistro.animaRegistro();
              }else{
                return Container();
              }
            }
          ),
        ],
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueAccent,
        elevation: 0,
        title: Text(
          "Index",
          style: TextStyle(
              color: Colors.white
          ),
        )
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              fit: BoxFit.fill,
              image: AssetImage("assets/images/01.jpg")
          )
        ),
        padding: EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[
            Center(child: _logo()),
            Expanded(child: _index()),
          ],
        )
      ),
    );
  }
}