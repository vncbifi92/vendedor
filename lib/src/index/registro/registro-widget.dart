import 'package:flutter/material.dart';
import 'package:vendedor/src/index/registro/registro-bloc.dart';
import 'package:vendedor/src/index/index-bloc.dart' as indexBloc;
import 'package:vendedor/src/produtos/index-widget.dart';
import 'package:vendedor/src/produtos/index-bloc.dart';

class Registro {

  final formKey = GlobalKey<FormState>();

  String _nome, _senha, _email;

  _registra(BuildContext context)async{

    if(formKey.currentState.validate()) {
      formKey.currentState?.save();

      int usuarioId = await registroBloc.registro(_nome, _senha, _email);
      await produtosBloc.carregaMaisVendidos(usuarioId);
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              settings: const RouteSettings(name: '/produtosIndex'),
              builder: (context) => ProdsPage(
                usuarioId: usuarioId,
              )
          )
      );
    }
  }

  _switchAnimarRegistro(){
    registroBloc.switchAnimar();
    indexBloc.indexBloc.switchAnimar();
  }

  Widget _formRegistro(BuildContext context){
    return SingleChildScrollView(
      child: Center(
        child: Form(
            key: formKey,
            child: Padding(
              padding: const EdgeInsets.all(50.0),
              child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Title(color: Colors.white, child: Text("Registro")),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: StreamBuilder<Object>(
                          stream: registroBloc.outAnimar,
                          builder: (context, snapshot) {
                            return TextFormField(
                                validator: (value){
                                  if(value.length < 3){
                                    return "Este campo deve conter no minimo 3 caracteres.";
                                  }
                                },
                              onSaved: (val){
                                _nome = val;
                              },
                              enabled: snapshot.hasData ? snapshot.data : true,
                              decoration: InputDecoration(
                                  labelText: "Nome",
                                  border: UnderlineInputBorder()),
                            );
                          }
                      ),
                    ),
                      Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: StreamBuilder<Object>(
                          stream: registroBloc.outAnimar,
                          builder: (context, snapshot) {
                            return TextFormField(
                              validator: (value){
                                if(value.length < 3){
                                  return "Este campo deve conter no minimo 3 caracteres.";
                                }
                              },
                              onSaved: (val){
                                _email = val;
                              },
                              keyboardType: TextInputType.emailAddress,
                              enabled: snapshot.hasData ? snapshot.data : true,
                              decoration: InputDecoration(
                                  labelText: "Email",
                                  border: UnderlineInputBorder()),
                            );
                          }
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: StreamBuilder<Object>(
                          stream: registroBloc.outAnimar,
                          builder: (context, snapshot) {
                            return TextFormField(
                              validator: (value){
                                if(value.length < 3){
                                  return "Este campo deve conter no minimo 3 caracteres.";
                                }
                              },
                              onSaved: (val){
                                _senha = val;
                              },
                              obscureText: true,
                              enabled: snapshot.hasData ? snapshot.data : true,
                              decoration: InputDecoration(
                                  labelText: "Senha",
                                  border: UnderlineInputBorder()),
                            );
                          }
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: MaterialButton(
                              child: Text("Ok"),
                              color: Colors.lightBlue,
                              onPressed: (){_registra(context);}
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: MaterialButton(
                              child: Text("Voltar"),
                              color: Colors.lightBlue,
                              onPressed: _switchAnimarRegistro
                          ),
                        )
                      ],
                    ),
                  ]
              ),
            )
        ),
      ),
    );
  }

  Widget _botaoRegistro(){
    return Center(
      child: Text(
        "Registrar",
        style: TextStyle(color: Colors.white),
      ),
    );
  }

  Widget animaRegistro(){
    return StreamBuilder<Object>(
        stream: registroBloc.outAnimar,
        builder: (context, snapshot) {
          if(snapshot.hasData) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                child: GestureDetector(
                    onTap: snapshot.data ? null : _switchAnimarRegistro,
                    child: AnimatedContainer(
                      duration: Duration(milliseconds: 500),
                      height: snapshot.data ? 400 : 50,
                      decoration: BoxDecoration(
                          image: snapshot.data ? DecorationImage(image: AssetImage("assets/images/05.jpeg"), fit: BoxFit.cover) : null,
                          color: snapshot.data ? null : Colors.green,
                          borderRadius: BorderRadius.circular(20)
                      ),
                      child: AnimatedCrossFade(
                          firstChild: _formRegistro(context),
                          secondChild: _botaoRegistro(),
                          crossFadeState: snapshot.data
                              ? CrossFadeState.showFirst
                              : CrossFadeState.showSecond,
                          duration: Duration(milliseconds: 500)
                      ),
                    )
                ),
              ),
            );
          }else{
            return Container();
          }
        }
    );
  }

}

Registro widgetRegistro = Registro();