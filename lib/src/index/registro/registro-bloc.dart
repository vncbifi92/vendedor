import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/rxdart.dart';
import 'package:vendedor/src/shared/service/database.dart';

class RegistroBloc extends BlocBase{

  var _animar = BehaviorSubject<bool>.seeded(false);

  Stream<bool> get outAnimar => _animar.stream;

  Sink<bool> get inAnimar => _animar.sink;

  bool get animarValue => _animar.value;

  void salvaAnimar(bool animar){
    inAnimar.add(animar);
    print("animar registro $animar");
  }

  void switchAnimar(){
    salvaAnimar(!animarValue);
  }

  @override
  void dispose() {
    _animar.close();
  }

  Future<int> registro(String nome, String senha, String email) async{

    UsuarioData usuario = UsuarioData(nome: nome, senha: senha, email: email);

    return await MyDatabase.instance.addUsuario(usuario);

  }

}

RegistroBloc registroBloc = RegistroBloc();