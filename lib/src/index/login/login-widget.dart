import 'package:flutter/material.dart';
import 'package:vendedor/src/index/login/login-bloc.dart';
import 'package:vendedor/src/index/index-bloc.dart' as indexBloc;
import 'package:vendedor/src/produtos/index-bloc.dart';
import 'package:vendedor/src/produtos/index-widget.dart';
import 'package:vendedor/src/shared/service/database.dart';

class Login {

  final formKey = GlobalKey<FormState>();
  String _nome;
  String _senha;


  _login(BuildContext context)async{
    if(formKey.currentState.validate()) {
      formKey.currentState?.save();
      UsuarioData usuario = await loginBloc.login(_nome, _senha);
      await produtosBloc.carregaMaisVendidos(usuario.id);
      if (usuario != null) {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                settings: const RouteSettings(name: '/produtosIndex'),
                builder: (context) => ProdsPage(
                  usuarioId: usuario.id,
                )
            )
        );
      } else {
        showDialog(context: context,
            barrierDismissible: true,
            builder: (BuildContext context) {
              return AlertDialog(
                content: Text("Usuario e/ou senha invalido."),
                actions: <Widget>[
                  FlatButton(
                    color: Colors.transparent,
                    child: Text(
                      "Ok",
                      style: TextStyle(
                          color: Colors.orange
                      ),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  )
                ],
              );
            }
        );
      }
    }

  }
  
  _switchAnimarLogin(){
    loginBloc.switchAnimar();
    indexBloc.indexBloc.switchAnimar();
  }

  Widget _formLogin(BuildContext context){
    return SingleChildScrollView(
      child: Center(
        child: Form(
            key: formKey,
            child: Padding(
              padding: const EdgeInsets.all(50.0),
              child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Title(color: Colors.white, child: Text("Login")),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: StreamBuilder<Object>(
                          stream: loginBloc.outAnimar,
                          builder: (context, snapshot) {
                            return TextFormField(
                              onSaved: (String val){
                                _nome = val;
                              },
                              cursorColor: Colors.white,
                              enabled: snapshot.hasData ? snapshot.data : true,
                              decoration: InputDecoration(
                                  labelText: "Nome",
                                  border: UnderlineInputBorder()),
                            );
                          }
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: StreamBuilder<Object>(
                          stream: loginBloc.outAnimar,
                          builder: (context, snapshot) {
                            return TextFormField(
                              onSaved: (String val){
                                _senha = val;
                              },
                              obscureText: true,
                              enabled: snapshot.hasData ? snapshot.data : true,
                              decoration: InputDecoration(
                                  labelText: "Senha",
                                  border: UnderlineInputBorder()),
                            );
                          }
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: MaterialButton(
                              child: Text("Ok"),
                              color: Colors.lightBlueAccent,
                              onPressed: (){_login(context);}
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: MaterialButton(
                              child: Text("Voltar"),
                              color: Colors.lightBlueAccent,
                              onPressed: _switchAnimarLogin
                          ),
                        )
                      ],
                    ),
                  ]
              ),
            )
        ),
      ),
    );
  }

  Widget _botaoLogin(){
    return Center(
      child: Text(
        "Login",
        style: TextStyle(color: Colors.white),
      ),
    );
  }

  Widget animaLogin(){
    return StreamBuilder<Object>(
        stream: loginBloc.outAnimar,
        builder: (context, snapshot) {
          if(snapshot.hasData) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                child: GestureDetector(
                    onTap: snapshot.data ? null : _switchAnimarLogin,
                    child: AnimatedContainer(
                      duration: Duration(milliseconds: 500),
                      height: snapshot.data ? 400 : 50,
                      decoration: BoxDecoration(
                        image: snapshot.data ? DecorationImage(image: AssetImage("assets/images/04.jpeg"), fit: BoxFit.cover) : null,
                        color: snapshot.data ? null : Colors.blue,
                        borderRadius: BorderRadius.circular(20)
                      ),
                      child: AnimatedCrossFade(
                          firstChild: _formLogin(context),
                          secondChild: _botaoLogin(),
                          crossFadeState: snapshot.data
                              ? CrossFadeState.showFirst
                              : CrossFadeState.showSecond,
                          duration: Duration(milliseconds: 500)
                      ),
                    )
                ),
              ),
            );
          }else{
            return Container();
          }
        }
    );
  }

}

Login widgetLogin = Login();