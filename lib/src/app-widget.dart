import 'package:flutter/material.dart';
import 'package:vendedor/src/shared/routes.dart';

class AppWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      routes: routes,
      debugShowCheckedModeBanner: false,
    );
  }
}