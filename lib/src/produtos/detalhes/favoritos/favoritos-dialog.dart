import 'package:flutter/material.dart';
import 'package:vendedor/src/shared/models/produtos.dart';

class Dialogs{

  detalheFavoritos(BuildContext context, Produto produto) {

    Widget _detalheProdutos(){
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Image.network(produto.foto),
          Text(produto.nota),
        ],
      );
    }

    Widget _voltarButton(){
      return FlatButton(
        color: Colors.transparent,
        child: Text(
          "Voltar",
          style: TextStyle(
              color: Colors.orange
          ),
        ),
        onPressed: (){Navigator.pop(context);},
      );
    }

    return  showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return AlertDialog(
            title: Text(produto.titulo),
            content: _detalheProdutos(),
            actions: <Widget>[
              _voltarButton()
            ],
          );
        }
    );
  }

}