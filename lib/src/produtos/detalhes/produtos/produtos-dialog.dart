import 'package:flutter/material.dart';
import 'package:vendedor/src/shared/service/database.dart';

class Dialogs{

  detalheProdutos(BuildContext context, Produto produto) {

    Widget _detalheProdutos(){
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(                  
            padding: const EdgeInsets.all(8.0),
            child: Container(
              child: Image.asset(
                  produto.foto,
                  fit: BoxFit.scaleDown
              ),
              height: 100,
              width: 100,
            ),
          ),
          Text(produto.nota),
        ],
      );
    }

    Widget _voltarButton(){
      return FlatButton(
        color: Colors.transparent,
        child: Text(
          "Voltar",
          style: TextStyle(
              color: Colors.orange
          ),
        ),
        onPressed: (){Navigator.pop(context);},
      );
    }

    return  showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return AlertDialog(
            title: Text(produto.titulo),
            content: _detalheProdutos(),
            actions: <Widget>[
              _voltarButton()
            ],
          );
        }
    );
  }

}