import 'package:flutter/material.dart';
import 'package:vendedor/src/produtos/busca/busca-bloc.dart';
import 'package:vendedor/src/shared/service/database.dart' as db;
import 'package:vendedor/src/produtos/detalhes/produtos/produtos-dialog.dart' as produtos;

class BuscaProdsPage extends StatefulWidget {
  BuscaProdsPage({Key key, this.usuarioId}) : super(key: key);

  final int usuarioId;

  @override
  _BuscaProdsPageState createState() => new _BuscaProdsPageState();
}

class _BuscaProdsPageState extends State<BuscaProdsPage>{

  String _nome = "";
  final formKey = GlobalKey<FormState>();

  _buscaProdutos(){
    if(formKey.currentState.validate()){
      formKey.currentState?.save();
      buscaProdutosBloc.buscaProdutos(_nome);
    }
  }

  _detalheProduto(db.Produto produto){

    produtos.Dialogs dialogs = new produtos.Dialogs();
    dialogs.detalheProdutos(context, produto);
  }

  Widget _listaProdutos(){
    return StreamBuilder(
        stream: buscaProdutosBloc.outProduto,
        builder: (BuildContext context, AsyncSnapshot<List<db.Produto>> snapshot) {
          if (snapshot.hasData) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                child: ListView.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    physics: const ClampingScrollPhysics(),
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        child: ListTile(
                          leading: CircleAvatar(
                              backgroundImage: AssetImage(snapshot.data.elementAt(index).foto)
                          ),
                          title: Text(
                              snapshot.data.elementAt(index).titulo
                          ),
                          subtitle: Text(
                              snapshot.data.elementAt(index).nota
                          ),
                        ),
                        onTap: (){_detalheProduto(snapshot.data.elementAt(index));},
                      );
                    }
                ),
              ),
            );
          }else{
            return Container();
          }
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueAccent,
        elevation: 0,
        title: Text(
          "Buscar Produtos",
          style: TextStyle(
              color: Colors.white
          ),
        )
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Form(
              key: formKey,
              child: Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: TextFormField(
                          onSaved: (String val){
                            _nome = val;
                          },
                          cursorColor: Colors.white,
                          decoration: InputDecoration(
                              labelText: "O que esta procurando?",
                              border: UnderlineInputBorder()),
                        ),
                      ),
                      IconButton(icon: Icon(Icons.search), onPressed: (){_buscaProdutos();})
                    ],
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
                child: Text(
                  "Produtos:",
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
                child: _listaProdutos(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}