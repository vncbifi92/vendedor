import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/rxdart.dart';
import 'package:vendedor/src/shared/service/database.dart' as db;

class BuscaProdutosBloc extends BlocBase{

  var _produtos = BehaviorSubject<List<db.Produto>>.seeded([]);

  Stream<List<db.Produto>> get outProduto => _produtos.stream;

  Sink<List<db.Produto>> get inProduto => _produtos.sink;

  List<db.Produto> get produtoValue => _produtos.value;

  void salvaProduto(List<db.Produto> produtos){
    inProduto.add(produtos);
    print("produtos $produtos");
  }

  void buscaProdutos(String nome) async{
    salvaProduto(await db.MyDatabase.instance.getProdutosPorNome(nome));
  }

  @override
  void dispose() {
    _produtos.close();
  }

}

BuscaProdutosBloc buscaProdutosBloc = BuscaProdutosBloc();