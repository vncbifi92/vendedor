import 'package:flutter/material.dart';
import 'package:vendedor/src/produtos/index-bloc.dart';
import 'package:vendedor/src/shared/models/produtos.dart';
import 'package:vendedor/src/shared/service/database.dart' as db;
import 'detalhes/favoritos/favoritos-dialog.dart' as favoritos;
import 'detalhes/produtos/produtos-dialog.dart' as produtos;
import 'novos/novos-widget.dart';

class ProdsPage extends StatefulWidget {
  ProdsPage({Key key, this.usuarioId}) : super(key: key);

  final int usuarioId;

  @override
  _ProdsPageState createState() => new _ProdsPageState();
}

class _ProdsPageState extends State<ProdsPage>{

  _detalheProdutoWS(Produto produto){

    favoritos.Dialogs dialogs = new favoritos.Dialogs();
    dialogs.detalheFavoritos(context, produto);
  }

  _detalheProduto(db.Produto produto){

    produtos.Dialogs dialogs = new produtos.Dialogs();
    dialogs.detalheProdutos(context, produto);
  }

  _removeProduto(db.Produto produto) async{
    produtosBloc.removeProduto(produto);
  }

  _novoProduto() async{
    Navigator.push(
        context,
        MaterialPageRoute(
            settings: const RouteSettings(name: '/novoProduto'),
            builder: (context) => NovoProdPage(
              usuarioId: widget.usuarioId,
            )
        )
    );
  }

  Widget _listaProdutos(){
    return StreamBuilder(
      stream: db.MyDatabase.instance.getProdutos(widget.usuarioId),
      builder: (BuildContext context, AsyncSnapshot<List<db.Produto>> snapshot) {
        if (snapshot.hasData) {
          return Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  physics: const ClampingScrollPhysics(),
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      child: ListTile(
                        leading: CircleAvatar(
                          backgroundImage: AssetImage(snapshot.data.elementAt(index).foto)
                        ),
                        title: Text(
                          snapshot.data.elementAt(index).titulo
                        ),
                        subtitle: Text(
                          snapshot.data.elementAt(index).nota
                        ),
                      ),
                      onTap: (){_detalheProduto(snapshot.data.elementAt(index));},
                      onLongPress: () => _removeProduto(snapshot.data.elementAt(index)),
                    );
                  }
                ),
              ),
            ),
          );
        }else{
          return Container();
        }
      }
    );
  }

  Widget _listaProdutosWS(){
    return StreamBuilder(
      stream: produtosBloc.outProdutoWS,
      builder: (BuildContext context, AsyncSnapshot<List<Produto>> snapshot) {
        if (snapshot.hasData) {
          return Container(
            height: 150,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              physics: const ClampingScrollPhysics(),
              itemCount: snapshot.data.length,
              itemBuilder: (context, index) {
                return Padding(
                  padding: EdgeInsets.all(8),
                  child: GestureDetector(
                    child: Image.network(snapshot.data.elementAt(index).foto,),
                    onTap: (){_detalheProdutoWS(snapshot.data.elementAt(index));},
                  )
                );
              }
            ),
          );
        }else{
          return Container();
        }
      }
    );
  }


  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          IconButton(icon: Icon(Icons.search), onPressed: (){Navigator.pushNamed(context, "/produtoBusca");}),
          IconButton(icon: Icon(Icons.exit_to_app), onPressed: (){Navigator.pushReplacementNamed(context, "/");})
        ],
        backgroundColor: Colors.blueAccent,
        elevation: 0,
        title: Text(
          "Produtos",
          style: TextStyle(
              color: Colors.white
          ),
        )
      ),
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
            child: Text(
              "Favoritos:",
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            decoration: BoxDecoration(
              border: Border.all(color: Colors.redAccent)
            ),
            child: _listaProdutosWS()
          ),
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
            child: Text(
              "Meus produtos:",
            ),
          ),
        ),
        _listaProdutos(),
        ],
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          backgroundColor: Colors.lightBlueAccent,
          onPressed: (){_novoProduto();}
      ),
    );
  }
}