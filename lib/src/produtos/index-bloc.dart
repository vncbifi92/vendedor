import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/rxdart.dart';
import 'package:vendedor/src/shared/models/produtos.dart';
import 'package:vendedor/src/shared/service/database.dart' as db;
import 'package:dio/dio.dart';
import 'dart:convert';

class ProdutosBloc extends BlocBase{

  static final String TOKEN = "kpWGoa8ra-g1h4l1MZzsUw";
  static final String URL = "https://app.fakejson.com/q/6HCcnlnm?token=$TOKEN";


  var _animar = BehaviorSubject<bool>.seeded(false);

  Stream<bool> get outAnimar => _animar.stream;

  Sink<bool> get inAnimar => _animar.sink;

  bool get animarValue => _animar.value;

  void salvaAnimar(bool animar){
    inAnimar.add(animar);
    print("animar produtos $animar");
  }

  void switchAnimar() {
    salvaAnimar(!animarValue);
  }

  var _produtos = BehaviorSubject<List<db.Produto>>.seeded([]);

  Stream<List<db.Produto>> get outProduto => _produtos.stream;

  Sink<List<db.Produto>> get inProduto => _produtos.sink;

  List<db.Produto> get produtoValue => _produtos.value;

  void salvaProduto(List<db.Produto> produtos){
    inProduto.add(produtos);
    print("produtos $produtos");
  }

  void addProduto(db.Produto produto){

    db.MyDatabase.instance.addProduto(produto);
  }

  var _produtosWS = BehaviorSubject<List<Produto>>.seeded([]);

  Stream<List<Produto>> get outProdutoWS => _produtosWS.stream;

  Sink<List<Produto>> get inProdutoWS => _produtosWS.sink;

  List<Produto> get produtoValueWS => _produtosWS.value;

  void salvaProdutoWS(List<Produto> produtosWS){
    inProdutoWS.add(produtosWS);
    print("produtosWS $produtosWS");
  }

  void carregaMaisVendidos(int usuarioId) async{
    var dio = Dio();
    Response response = await dio.get(URL);

    print(response.data.toString());

    Data json = Data.fromJson(response.data);

    List<Produto> produtos = [];

    for(Produto produto in json.produtos){
                                                                                  
      produtos.add(Produto(titulo: produto.titulo, nota: produto.nota, data: produto.data, foto: produto.foto));

    }

    salvaProdutoWS(produtos);
  }


  void removeProduto(db.Produto produto){
    db.MyDatabase.instance.removeProduto(produto);
  }



  @override
  void dispose() {
    _produtos.close();
    _animar.close();
  }

}

ProdutosBloc produtosBloc = ProdutosBloc();