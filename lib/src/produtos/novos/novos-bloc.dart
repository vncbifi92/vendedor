import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/rxdart.dart';

class NovosBloc extends BlocBase{

  var _imagem = BehaviorSubject<String>.seeded("");

  Stream<String> get outImagem => _imagem.stream;

  Sink<String> get inImagem => _imagem.sink;

  String get imagemValue => _imagem.value;

  void salvaImagem(String imagem){
    inImagem.add(imagem);
    print("imagem $imagem");
  }

  var _titulo = BehaviorSubject<String>.seeded("");

  Stream<String> get outTitulo => _titulo.stream;

  Sink<String> get inTitulo => _titulo.sink;

  String get tituloValue => _titulo.value;

  void salvaTitulo(String titulo){
    inTitulo.add(titulo);
    print("titulo $titulo");
  }

  var _nota = BehaviorSubject<String>.seeded("");

  Stream<String> get outNota => _nota.stream;

  Sink<String> get inNota => _nota.sink;

  String get notaValue => _nota.value;

  void salvaNota(String nota){
    inNota.add(nota);
    print("nota $nota");
  }

  @override
  void dispose() {
    _imagem.close();
    _titulo.close();
    _nota.close();
  }

  void reset(){
    salvaImagem("");
    salvaNota("");
    salvaTitulo("");
  }

}

NovosBloc novosBloc = NovosBloc();