import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:vendedor/src/produtos/index-bloc.dart';
import 'package:vendedor/src/shared/service/database.dart' as db;

import 'novos-bloc.dart';

class NovoProdPage extends StatefulWidget {
  NovoProdPage({Key key, this.usuarioId}) : super(key: key);

  final int usuarioId;

  @override
  _NovoProdPageState createState() => new _NovoProdPageState();
}

class _NovoProdPageState extends State<NovoProdPage>{

  final formKey = GlobalKey<FormState>();

  void _getImage(BuildContext context) {

    ImagePicker.pickImage(
      source: ImageSource.gallery,
      maxWidth: 400.0,
      maxHeight: 400.0,
    ).then((File image) {
      novosBloc.salvaImagem(image.path);

    });
  }

  _registraProduto(){

    if(formKey.currentState.validate()){
      formKey.currentState?.save();
      db.Produto produto = db.Produto(
        data: DateTime.now(),
        nota: novosBloc.notaValue,
        titulo: novosBloc.tituloValue,
        usuarioId: widget.usuarioId,
        foto: novosBloc.imagemValue
      );
      produtosBloc.addProduto(produto);
      novosBloc.reset();
      Navigator.pop(context);
    }
  }

  Widget _formProduto(){

    return SingleChildScrollView(
      child: Center(
        child: Form(
            key: formKey,
            child: Padding(
              padding: const EdgeInsets.all(50.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    child: GestureDetector(
                      onTap: (){_getImage(context);},
                      child: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        radius: 100,
                        child: StreamBuilder<String>(
                            stream: novosBloc.outImagem,
                            builder: (context, snapshot) {
                              if(snapshot.hasData){
                                if(snapshot.data.trim().isNotEmpty){
                                  return Image.asset(snapshot.data.toString());
                                }else{
                                  return Container(
                                    height: 50,
                                    alignment: Alignment.center,
                                    color: Colors.lightBlue,
                                    child: Text("Carrega imagem", style: TextStyle(color: Colors.black45),)
                                  );
                                }
                              }
                            }
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child:TextFormField(
                      validator: (value){
                        if(value.length < 3){
                          return "Este campo deve conter no minimo 3 caracteres.";
                        }
                      },
                      onSaved: (val){
                        novosBloc.salvaTitulo(val.toString());
                      },
                      decoration: InputDecoration(
                          labelText: "Nome",
                          border: UnderlineInputBorder()),
                    )
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      validator: (value){
                        if(value.length < 3){
                          return "Este campo deve conter no minimo 3 caracteres.";
                        }
                      },
                      onSaved: (val){
                        novosBloc.salvaNota(val.toString());
                      },
                      decoration: InputDecoration(
                          labelText: "Descrição",
                          border: UnderlineInputBorder()),
                    )
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: MaterialButton(
                            child: Text("Ok"),
                            color: Colors.lightBlue,
                            onPressed: (){_registraProduto();}
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: MaterialButton(
                            child: Text("Voltar"),
                            color: Colors.lightBlue,
                            onPressed: (){Navigator.pop(context);novosBloc.reset();}
                        ),
                      )
                    ],
                  ),
                ]
              ),
            )
        ),
      ),
    );

  }

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.blueAccent,
          elevation: 0,
          title: Text(
            "Novo Produto",
            style: TextStyle(
                color: Colors.white
            ),
          )
      ),
      body: _formProduto(),
    );
  }
}